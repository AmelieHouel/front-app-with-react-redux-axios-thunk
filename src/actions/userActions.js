import axios from "axios";
import { GET_ERRORS, SET_CURRENT_USER } from "./types";
import setJWTToken from "../utils/setJWTToken";
import jwt_decode from "jwt-decode";

export const register = (user, history) => async (dispatch) => {
  try {
    const res = await axios.post("http://127.0.0.1:8000/api/users", user);
    console.log(res);
    if (res.data.success === 0) {
      dispatch({
        type: GET_ERRORS,
        payload: res.data,
      });
    } else {
      history.push("/login");
    }
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err,
    });
  }
};

export const login = (LoginRequest) => async (dispatch) => {
  try {
    const res = await axios.post(
      "http://127.0.0.1:8000/api/users/login",
      LoginRequest
    );
    // Extraction du Token
    const { token } = res.data;
    //Stockage du token en localStorage
    localStorage.setItem("jwtToken", token);
    setJWTToken(token);
    const decoded = jwt_decode(token);
    dispatch({
      type: SET_CURRENT_USER,
      payload: decoded,
    });
  } catch (err) {
    dispatch({
      type: GET_ERRORS,
      payload: err,
    });
  }
};

export const logout = () => (dispatch) => {
  localStorage.removeItem("jwtToken");
  setJWTToken(false);
  dispatch({
    type: SET_CURRENT_USER,
    payload: {},
  });
};
