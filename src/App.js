import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
//Components
import Dashboard from "./components/Dashboard";
import Register from "./components/user/Register";
import Header from "./components/layout/Header";
import Login from "./components/user/Login";
//css
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
//redux
import { Provider } from "react-redux";
import store from "./store";
import setJWTToken from "./utils/setJWTToken";
import { SET_CURRENT_USER } from "./actions/types";
import jwt_decode from "jwt-decode";
import { logout } from "./actions/userActions";
import SecureRoute from "./utils/SecureRoute";

// Récupération du token stocké en local
const jwtToken = localStorage.jwtToken;
if (jwtToken) {
  setJWTToken(jwtToken);
  const decodedJwtToken = jwt_decode(jwtToken);
  store.dispatch({
    type: SET_CURRENT_USER,
    payload: decodedJwtToken,
  });
  const currentTime = Date.now() / 1000;
  if (decodedJwtToken.exp < currentTime) {
    store.dispatch(logout());
    window.location.href = "/";
  }
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <Header />
            {
              // Public Routes
              
            }
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />

            {
              // Private Routes
            }
            <Switch>
            <SecureRoute exact path="/dashboard" component={Dashboard} />
            </Switch>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
