import React, { Component } from "react";
import { connect } from "react-redux";
import { register } from "../../actions/userActions";
import PropTypes from "prop-types";

class Register extends Component {
  constructor() {
    super();
    this.state = {
      firstname: "",
      lastname: "",
      gender: "",
      email: "",
      password: "",
      phonenumber: "",
      errors: {},
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  onSubmit(e) {
    e.preventDefault();
    const newUser = {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      gender: this.state.gender,
      email: this.state.email,
      password: this.state.password,
      phonenumber: this.state.phonenumber,
    };
    //console.log(newUser);
    this.props.register(newUser, this.props.history);
  }

  componentWillReceiveProps(nextProps) {
    console.log("Next Props :", nextProps);
    if (nextProps.errors.success === 0) {
      this.setState({ errors: nextProps.errors });
    } else if(nextProps.errors.response.data.success === 0) {
      this.setState({ errors: nextProps.errors.response.data })
    }
  }

  render() {
    const { errors } = this.state;
    return (
      <div className="register">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Inscription</h1>
              <p className="lead text-center">Créer votre compte</p>
    {errors.success === 0 && (<div className="alert alert-danger">{errors.message}</div>)}
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg"
                    placeholder="Votre prénom"
                    name="firstname"
                    value={this.state.firstname}
                    onChange={this.onChange}
                    required
                  />
                </div>

                <div className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg"
                    placeholder="Votre nom de famille"
                    name="lastname"
                    value={this.state.lastname}
                    onChange={this.onChange}
                    required
                  />
                </div>

                <div className="form-group">
                  <select
                    name="gender"
                    value={this.state.gender}
                    onChange={this.onChange}
                    className="form-control form-control-lg"
                  >
                    <option value="Feminin">Feminin</option>
                    <option value="Masculin">Masculin</option>
                  </select>
                </div>

                <div className="form-group">
                  <input
                    type="email"
                    className="form-control form-control-lg"
                    placeholder="Votre Adresse mail"
                    name="email"
                    value={this.state.email}
                    onChange={this.onChange}
                  />
                </div>

                <div className="form-group">
                  <input
                    type="password"
                    className="form-control form-control-lg"
                    placeholder="Votre Mot de passe"
                    name="password"
                    value={this.state.password}
                    onChange={this.onChange}
                  />
                </div>

                <div className="form-group">
                  <input
                    type="text"
                    className="form-control form-control-lg"
                    placeholder="Votre numéro de téléphone"
                    name="phonenumber"
                    value={this.state.phonenumber}
                    onChange={this.onChange}
                  />
                </div>

                <input type="submit" className="btn btn-info btn-block mt-4" />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
Register.propTypes = {
  register: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  errors: state.errors,
})
export default connect(mapStateToProps, { register })(Register);
