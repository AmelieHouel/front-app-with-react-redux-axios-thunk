import React, { Component } from "react";
// import UserItem from "./user/UserItem";
class Dashboard extends Component {
  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h1 className="display-4 text-center">Projet</h1>
              <br />
              <a href="ProjectForm.html" className="btn btn-lg btn-info">
                Créer un projet
              </a>
              <br />
              <hr />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Dashboard;
